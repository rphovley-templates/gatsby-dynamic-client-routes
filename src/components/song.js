import React from "react"

export default function SongPage({ id }) {
  return (
    <div>
      <p>Static Song content to be replaced {id}</p>
    </div>
  )
}
