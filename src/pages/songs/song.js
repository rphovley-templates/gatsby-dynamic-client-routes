import React from "react"

import { Router } from "@reach/router" // included with gatsby
import SongComponent from "../../components/song"
import Layout from "../../components/layout"

export default function SongPage() {
  return (
    <Layout>
      <Router>
        {/* Tells client router to load the song component when the path matches /songs/:id */}
        <SongComponent path="/songs/:id" />
      </Router>
    </Layout>
  )
}
