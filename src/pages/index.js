import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <p>Welcome to your Songs site!</p>
    <Link to="/songs">Go to the songs</Link>
  </Layout>
)

export default IndexPage
