import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const songs = [
  {
    id: 1,
    name: "Falling",
  },
  {
    id: 2,
    name: "Paranoid",
  },
  {
    id: 3,
    name: "Mega Tubes",
  },
  {
    id: 4,
    name: "Hey Jude",
  },
  {
    id: 5,
    name: "Paralyzed",
  },
]
const SongsPage = props => (
  <Layout>
    <SEO title="Song Page" />
    <h1>Hi from the songs page</h1>
    <p>Welcome to the Songs page! ({props.path})</p>
    {songs.map(song => (
      <Link style={{ display: "block" }} to={`/songs/${song.id}`}>
        {song.name}
      </Link>
    ))}
  </Layout>
)

export default SongsPage
